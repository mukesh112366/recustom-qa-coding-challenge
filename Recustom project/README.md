# Playwright Automation Project

## Overview
This project contains automated tests written using Playwright to test the functionality of a web application.

## Prerequisites
- Node.js installed
- npm or yarn package manager

## Setup
1. Clone this repository
2. Install dependencies using `npm install`
3. Configure your environment variables (if necessary)
4. Run tests using `npm run test`

## Configuration
- The `playwright.config.js` file contains configuration for Playwright, which is used to run the Playwright tests.

## Folder Structure
- `tests/`: Contains the actual test files written in Playwright.
- `pages/`: Contains Page Object classes to abstract away the interaction with web pages.

## Writing Tests
- Tests are written using Jest and Playwright.
- Use `pageObjects` to interact with the web pages instead of direct Playwright API calls within the test.

## Running Tests
- Tests can be run using the command `npm run test`.
- Tests are automatically launched in a headless Chromium browser by default. To run tests in a headed browser, set the `HEADLESS` environment variable to false before running the npm test command

## CI/CD Integration
- This project can be integrated into a CI/CD pipeline for automated testing.
- Ensure the necessary dependencies are installed and the correct test command is executed in the pipeline configuration.

## Reporting
- Playwright generates detailed test reports by default, which can be found in the `./test-results` and `./playwright-report` directory after running the tests.

