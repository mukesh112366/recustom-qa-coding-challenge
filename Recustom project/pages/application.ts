import { Page } from "@playwright/test";
import searchPage from "./searchPage";

export default class Application {
    protected page: Page;
    searchPage: searchPage;

    constructor(page: Page) {
        this.page = page;
        this.searchPage = new searchPage(page);
    };
};
