import { Locator, Page, expect } from "@playwright/test";
import Base from "./base";

export default class searchPage extends Base {
    constructor(page: Page) {
        super(page);
        this.page = page;
    }

    selectors = {
        searchIcon: "//span[@class='icon-search text-lg leading-none']",
        searchField: "//input[@id='field']",
        searchButton: "//div[text()=' Search ']",
        copyLinkIcon: ".copy-link-svg",
        copyUrlButton: "//div[text()='Copy']",
        shareUrlField: "//div[@id='copyInput']",
        shareOnFacebookButton: "[alt='facebook']",
        shareOnTwitterButton: "[alt='twitter']",
        shareOnLinkedinButton: "[alt='linkedin']"
    };

    get searchIcon(): Locator {
        return this.page.locator(this.selectors.searchIcon);
    }

    get searchField(): Locator {
        return this.page.locator(this.selectors.searchField);
    }

    get searchButton(): Locator {
        return this.page.locator(this.selectors.searchButton);
    }

    get copyLinkIcon(): Locator {
        return this.page.locator(this.selectors.copyLinkIcon);
    }

    get copyUrlButton(): Locator {
        return this.page.locator(this.selectors.copyUrlButton).nth(0);
    }

    get shareUrlField(): Locator {
        return this.page.locator(this.selectors.shareUrlField);
    }

    async clickOnSearchIcon(): Promise<void> {
        await this.page.waitForLoadState();
        await expect(this.searchIcon).toBeVisible();
        await this.searchIcon.click();
    }

    async enterSearchValue(value: string): Promise<void> {
        await expect(this.searchField).toBeVisible();
        await this.searchField.click();
        await this.searchField.fill(value);
    }

    async clickOnSearchButton(): Promise<void> {
        await expect(this.searchButton).toBeVisible();
        await this.searchButton.click();
    }

    async clickOnFirstCopyLinkIcon(): Promise<void> {
        await expect(this.copyLinkIcon.first()).toBeVisible();
        await this.copyLinkIcon.first().click();
    }

    async clickOnCopyButton(): Promise<void> {
        await expect(this.copyUrlButton).toBeVisible();
        await this.copyUrlButton.click();
        await this.page.waitForTimeout(1000);
    }

    async verifyUrlIsCopiedSuccessfully(): Promise<void> {
        await expect(this.shareUrlField).toBeVisible();
        await this.shareUrlField.innerText().then((text) => {
            expect(text).toEqual("Copied");
        });
        const clipboardContent = await this.page.evaluate(() => navigator.clipboard.readText());
        console.log(`Link - ${clipboardContent}`);
        expect(clipboardContent).not.toBeNull;
    }

    async verifyShareOnFacebookLink(searchValue: string): Promise<void> {
        const [multiPage] = await Promise.all([
            this.page.waitForEvent("popup"),
            this.page.click(this.selectors.shareOnFacebookButton)
        ]);
        await multiPage.waitForLoadState();
        await multiPage.locator('body').first().textContent().then((text) => {
            expect(text).toContain("Log in to your Facebook account to share.");
        })
        const url = multiPage.url();
        expect(url).toContain("https://www.facebook.com");
        expect(url).toContain(searchValue);
        await multiPage.close();
    }

    async verifyShareOnTwitterLink(searchValue: string): Promise<void> {
        const [multiPage] = await Promise.all([
            this.page.waitForEvent("popup"),
            this.page.click(this.selectors.shareOnTwitterButton)
        ]);
        await multiPage.waitForLoadState();
        await this.page.waitForTimeout(3000);
        await multiPage.locator('body').first().textContent().then((text) => {
            expect(text).toContain("You’ll need to log in before you can share that post.");
        })
        const url = multiPage.url();
        expect(url).toContain("https://twitter.com");
        expect(url).toContain(searchValue);
        await multiPage.close();
    }

    async verifyShareOnLinkedinLink(searchValue: string): Promise<void> {
        const [multiPage] = await Promise.all([
            this.page.waitForEvent("popup"),
            this.page.click(this.selectors.shareOnLinkedinButton)
        ]);
        await multiPage.waitForLoadState();
        await multiPage.locator('body').first().textContent().then((text) => {
            expect(text).toContain("Sign in");
        })
        const url = multiPage.url();
        expect(url).toContain("https://www.linkedin.com");
        expect(url).toContain(searchValue);
        await multiPage.close();
    }
};
