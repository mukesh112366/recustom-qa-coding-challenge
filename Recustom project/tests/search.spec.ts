import { Browser, BrowserContext, chromium, Page, test } from "@playwright/test";
import Application from "../pages/application";

let browser: Browser;
let context: BrowserContext;
let page: Page;
let App: Application;

const searchValue = "haggadah";

test.beforeAll(async () => {
  browser = await chromium.launch();
});

test.beforeEach(async () => {
  context = await browser.newContext();
  page = await context.newPage();
  App = new Application(page);
  await context.grantPermissions(['clipboard-read']);
  await page.goto("/");
});

test.afterEach(async () => {
  await page.close();
  await context.close();
});

test.afterAll(async () => {
  await browser.close();
});

test("Verify user is able to search and share the haggadah link through different channels", async () => {
  await App.searchPage.clickOnSearchIcon();
  await App.searchPage.enterSearchValue(searchValue);
  await App.searchPage.clickOnSearchButton();
  await App.searchPage.clickOnFirstCopyLinkIcon();
  await App.searchPage.clickOnCopyButton();
  await App.searchPage.verifyUrlIsCopiedSuccessfully();
  await App.searchPage.verifyShareOnFacebookLink(searchValue);
  await App.searchPage.verifyShareOnTwitterLink(searchValue);
  await App.searchPage.verifyShareOnLinkedinLink(searchValue);
});
