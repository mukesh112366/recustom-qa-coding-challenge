`TC01 - Searching and Sharing Haggadah`

`Preconditions:`
1. Access to a web browser with internet connectivity.
2. Access to Facebook, Twitter, and LinkedIn accounts for testing sharing functionality.

`Test Steps:`
1. Navigate to URL - https://www.haggadot.com
2. Click on the search icon.
3. Enter "Haggadah" on search field.
4. Click on search button.
5. Select any Haggadah from the search results.
5. Click on "Share" icon associated with the selected Haggadah.
6. A pop-up share window will open.
8. Verify if sharing options such as sharing a link, sharing on Facebook, Twitter, or LinkedIn are available.
9. Verify that each sharing option is functional and leads to the respective platform for sharing.
10.Log in to the respective social media platform (if not already logged in) and verify that the Haggadah is shared as a post on the selected social media platform.

`Expected Results:`
1. The search functionality should return relevant Haggadah based on the provided keyword.
2. Sharing options should include sharing a link and sharing on Facebook, Twitter, and LinkedIn.
3. The shared link should direct users to the specific Haggadah.
4. hared posts on social media should contain accurate information and a link to the Haggadah.

`Actual Results:`
1. Search functionality returns relevant Haggadah matching the keyword.
2. Sharing options function smoothly, allowing for link sharing and social media posts.
3. Link sharing generates accurate links to specific Haggadah.
4. Social media sharing creates posts with correct Haggadah information and links.
